[![PIPIO](public/images/title.png)](htts://pipio.gitlab.io)

*"little strokes fell great oaks" -- m'garu*

The `pipio` project is building a privacy-preserving messaging system.

* [website](https://pipio.gitlab.io)
