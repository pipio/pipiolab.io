# The Codispersal Protocol (CODL)

The purpose of this project repository is to describe, define and publish the 
communications protocol and associated message formats for developers, makers and
specialists to be able to adopt and implement `codispersal`.

## Repository Structure

The main source document of the Codispersal Protocol is `specification.md`.

Along with the standard, a schema of the data model is 
available as `schema.proto`. This artifact describes parts of the specification 
in a structured language (Protocol Buffers). This allows for interoperable 
implementation of the standard.