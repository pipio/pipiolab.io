```
               88               88
               ""               ""

  8b,dPPYba,   88  8b,dPPYba,   88   ,adPPYba,
  88P'    "8a  88  88P'    "8a  88  a8"     "8a
  88       d8  88  88       d8  88  8b       d8
  88b,   ,a8"  88  88b,   ,a8"  88  "8a,   ,a8"
  88`YbbdP"'   88  88`YbbdP"'   88   `"YbbdP"'
  88               88
  88               88
```

welcome and happy hacking!

# make

[make](https://www.gnu.org/software/make/) is building all our projects,
familiar with it [you should be](https://makefiletutorial.com/).

# hook

to ensure your commits are all good,
the following *precommit* [git hook](https://githooks.com/) you should consider.

*.git/hooks/precommit*

```
#!/usr/bin/env bash
cd $(git rev-parse --show-toplevel)
make precommit
```

# contributing

we share a common ownership of the code and we treat the each other contributions as our own.

## oath

we swear by ada lovelace, by alan turing, and by all the past and future hackers making them our witnesses,
that we will carry out, according to our ability and judgment, this oath and this indenture.

to hold our teachers in this art equal to our own parents; to make them partners in our livelihood;
to consider they family as our own brothers, and to teach them this art, if they want to learn it,
without fee or indenture; to impart precept, oral instruction, and all other instruction to our own children,
the children of our teachers, and to indentured pupils who have taken the hacker’s oath, but to nobody else.

we will use technology to help the planet according to our ability and judgment,
but never with a view to injury and wrong-doing.
neither will we administer a virus to anybody when asked to do so, nor will we suggest such a course.

into whatsoever projects we enter, we will enter to help the planet,
and we will abstain from all intentional wrong-doing and harm, especially from abusing the privacy of people.
and whatsoever we shall see or hear in the course of our vocation,
as well as outside our vocation in our intercourse with men, if it be what should not be published abroad,
we will never divulge, holding such things to be secrets.

now if we carry out this oath, and break it not,
may we gain for ever reputation among all for our life and for our art;
but if we break it and forswear ourself, may the opposite befall us.
