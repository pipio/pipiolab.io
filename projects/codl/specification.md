|Status   ||
|-------- |------------
| version |1
| status  |DRAFT 3
| date    |13 APR 2020

# 1 Introduction

## 1.1 Background

Current telecommunication systems consits of large networks of machines
interconnected through physical links. The protocols used to exchange
information through those links were not built with privacy in mind and expose
their participants to risks of mass surveillance and censorship.

One of such network, The Internet, does provides a means of exchanging messages
with low latency on a network of machines interconnected through physical links.

The overall infrastructure is provided and supported by industrial companies
restricting access to the subscribers of their commercial services, and selling
properitary hardware for connectivity.

The CODL protocol provides a means of corresponding anonymously with high
latency on a network of relays interconnected organically through human
carriers.

The overall infrastructure should be provided by independent participants of
the system, and implemented with open and accessible hardware.

## 1.2 Purpose

The purpose of the CODL protocol is to provide a privacy-preserving
correspondence network. It is the intention to provide a means of communication
to be used that is censorship resistant, reliable and accessible by anyone.

The purpose of this document is to describe and define the protocol and
communications protocol and associated message formats for developers, makers 
and specialists to be able to adopt and implement the CODL protocol.

## 1.3 Used Terminology

The following phrases in this specification must be interpreted as
follows:

-   ... MUST: indicates that something is mandatory, i.e. an absolute requirement

-   ... MUST BE ... IF ...: indicates that something is mandatory if some condition is met

-   ... MUST BE ... WHEN ...: indicates something is mandatory when something becomes true

-   ... MUST NOT ...: indicates that something is not allowed, i.e. an absolute prohibition

-   ... MAY ONLY ... IF ...: indicates something is only allowed if some condition is met

-   ... MAY ...: indicates that something is allowed

-   ... SHOULD: indicates something is strongly recommended

-   ... SHOULD NOT: indicates something is strongly not recommended

-   ... NEED NOT ... / ... NOT REQUIRED : indicates that something is not mandatory

An overview of definitions of the most important terms used in this
document can be found in Annex D.

## 1.4 License and Usage

All persons and organisations that contributed to the initial development
of the CODL protocol did so disinterestedly.

The CODL protocol specification is dedicated to the public domain under the
[Creative Commons CC0-1.0 Universal Public Domain Dedication](http://creativecommons.org/publicdomain/zero/1.0/)
statement, meaning that to the extent possible under law, the authors
and their organisations have waived all copyright and related or neighboring
rights to this work, allowing anyone to copy, modify, distribute and implement
the work, even for commercial purposes, all without asking permission.

In no way are the patent or trademark rights of any person affected by this
dedication to the public domain, nor are the rights that other persons may
have in the work or in how the work is used, such as publicity or privacy
rights. Unless expressly stated otherwise, the authors and their organisations
make no warranties about the work, and disclaim liability for all uses of the
work, to the fullest extent permitted by applicable law. When using or citing
the work, you should not imply endorsement by the authors or the affirmer.

## 1.5 Configuration Management

It is foreseen that this standard requires updates in the near future as
a result of validation testing and to ensure compatibility with emerging
cryptography practices, paradigms and standards. Therefore, for the time
being, the standard will remain under configuration control of the authors.
Requests and comments may be e-mailed to the authors:

-   P. Gioenas: `patagioenas (at) ctemplar . com`

In the meantime, a more sustainable process for configuration management
of this standard in line with its open character is under consideration.

# 2 Overview

## 2.1 Design Principles

The following principles are the basis for the CODL protocol:

-   the protocol is free and open, anyone can join at any time without
    permission;
-   the working of the protocol does not rely on any third party, i.e.
    there is no ownership of the network that is created with the
    protocol and no dependency on specific software or a system;
-   the protocol should preserve its users from the risks of mass surveillence
    and censorship;
-   the protocol is agnostic from the underlying physical layer;
-   implementation only requires the use of open standards.

## 2.2 Protocol Stack and Scope

The CODL protocol works on top of a physical layer to create a network for
trusted messaging that can be used by different applications.

The physical layer must provide low range broadcasting allowing reliable
devivery to nearby peers.

The protocol stack comprises the following 4 layers, from bottom to top:

-   The *Link Layer* ensures local connectivity and reliable broadcasting
    of data frames between a set of nearby peers connected by a physical layer.

-   The *Network Layer* is structuring and managing a multi-relay network,
    including addressing, routing capsules and traffic control.

-   The *Transport Layer* ensures the transmission of datagrams between
    addresses on a network.

-   The *Application Layer* ensures the translation of data between the
    networking service and the applications.

## 2.3 References

### 2.3.1 Technical Standards

#### 2.3.1.1 Cryptographic Standards

A. RFC 8032, Edwards-Curve Digital Signature Algorithm (EdDSA), January 2017, internet: <https://www.ietf.org/rfc/rfc8032.txt>

B. RFC 8452, AES-GCM-SIV: Nonce Misuse-Resistant Authenticated Encryption, April 2019, internet: <https://www.ietf.org/rfc/rfc8452.txt>

C. FIPS 202, SHA-3 Standard: Permutation-Based Hash and Extendable-Output Functions, August 2015, internet: <https://csrc.nist.gov/publications/detail/fips/202/final>

#### 2.3.1.2 Communication Standards

D. RFC 4122, A Universally Unique IDentifier (UUID) URN Namespace, July 2005, internet: <https://www.ietf.org/rfc/rfc4122.txt>

Z. RFC 1149, Standard for the Transmission of IP Datagrams on Avian Carriers, April 1999, internet: <https://www.ietf.org/rfc/rfc1149.txt>

## 2.4 High-level Functional Overview

### 2.4.1 Communication Functionality

The network is composed of mobile devices carried by living organism which are
exposed to the network as carriers, and static devices dedicated to the public
infrastructure exposed as relays.

While roaming over the network, devices join and leave vortices to cooperatively
broadast frames and propagate packets.

While participating in a vortex, their traffic is mixed with noise and onion
routing is used to prevent other participants and observers from gathering
information that could be used for traffic analysis.

### 2.4.2 Message Functionality

The user must create an address to exchange messages on the network. The
envelope of the address must be known prior to sending messages to that address.
The user could generate a flock key pair for his address, and must share it as
part of the envelope to its flock relatives. Every other peers owning the flock
private key can then carry messages that are routed specifically to this
address.

The envelope must include the list of exit relays where messages should be
delivered to be eventually picked up by the address owner or its flock
relatives.

While travelling and carrying messages, peers build and maintain collectively a
topological representation of the network that they infer from their
interactions with relays.

When a peer wants to send message to an address, it relies on its internal
representation of the network to find the best paths. The message is then
encapsulated in an onion and routed through the relays by random carriers until
it eventually reach its exit relay where it should be picked up by the recipient
or one of its flock relatives.

When a relay receive a capsule from an other relay it then propagate the inner
capsule to the most appropriate carriers for their next destination. It does
also produce and broadcast periodically statistics of his exchanges with nearby
relays.

Every carriers and relays share periodically parts of their representation of
the network in the form of trees having as trunk a specific relay. Those trees
once aggregated following a set of common rules forms a collective
representation of the topological network graph.

### 2.4.2 Address Anonymity Levels

We describe three operational modes that users must choose from when creating a
new address and they define how the address should be handled by the network
service.

#### 2.4.2.1 Stealth

TODO: Here we describe manual announcement of address, no carrier and no flock key

TODO: Here we explain a stealth user does not generate own trees only mix others

TODO: Here we explain usage of random contact identifier and its limitation

#### 2.4.2.2 Social

TODO: Here we explain how anonymity strengthen by sharing flock key with relatives

TODO: Here we explain the difference between allowing a friend for messaging (address book) or trusting for flock

#### 2.4.2.2 Transparent

TODO: Here we explain we allow anyone to carry using a singleton system-wide flock key-pair

# 3 Data Format

## 3.1 Link Layer

### 3.1.1 Cell

The cell is the basic unit of communication, a cell represents a set of frames
that are broadcasted by peers on a common communication channel.

The vortex represents a shared communication context between a set of peers,
it is identified by a string of bytes which are randomly generated by the
initiator of the vortex.

During each revolution of a vortex members must broadcast their cell once,
starting by the pole and followed by other members in no specific order.

|Size       |Field       |Usage
|-----------|------------|-----------------------------------------------------
|32 B       |`vortex`    |public key of the vortex
|8 B        |`revolution`|counter incremented at the end of each revolution
|32 B       |`member`    |member public key
|variable   |`cipher`    |a part of the vortex private key
|variable   |`members`   |members of the vortex as a list of member id
|4 B        |`pole`      |index of the pole in `members`
|variable   |`frames`    |list of frames
|64 B       |`signature` |the signature of the `member` for this cell

### 3.1.2 Frame

The frame contains encrypted information to be received by a specific
participant, it consits of a bucket and some metadata.

Its fields are encrypted with a nonce to make frames indistinguishable from
each other and from noise, the nonce must be paddied such that every frame of
the system have the exact same size.

Frames are recursive data structure forming onions that must be routed through
the vortex members, eventually reaching its receiver.

|Size       |Field       |Usage
|-----------|------------|-----------------------------------------------------
|variable   |`metadata`  |encrypted `Metadata`
|variable   |`bucket`    |encrypted `Bucket`

#### 3.1.2.1 Metadata

|Size       |Field       |Usage
|-----------|------------|-----------------------------------------------------
|>= 8 B     |`nonce`     |
|32 B       |`receiver`  |identifier of the public key of the receiver
|32 B       |`hash`      |SHA-256 hash of the unencrypted bucket

#### 3.1.2.1 Bucket

|Size       |Field       |Usage
|-----------|------------|-----------------------------------------------------
|>= 8 B     |`nonce`     |
|4 B        |`type`      |`EXIT` = 0 \| `ROUTE` = 1
|variable   |`bucket`    |

The representation of `bucket` depends on `type` value.

For `EXIT`, it represents a set of `Packet` (See Network Layer).
For `ROUTE`, it represents a `Frame` to be broadcasted.

## 3.2 Network Layer

### 3.2.1 Packet

The packet contains datagrams to be delivered to a peer or remote address, or
it contains an encrypted capsule to be received by a specific address.

Capsules are recursive data structure forming onions that must be routed through
the network participants, eventually reaching its recipient.

|Size       |Field       |Usage
|-----------|------------|-----------------------------------------------------
|>= 8 B     |`nonce`     |
|4 B        |`type`      |`DELIVER` = 0 \| `FORWARD` = 1
|variable   |`kernel`    |

The representation of `kernel` depends on `type` value.

For `DELIVER`, it represents a set of `Datagram`s (See Transport Layer).
For `FORWARD`, it represents a `Capsule` to be forwarded.

### 3.2.2 Capsule

|Size       |Field       |Usage
|-----------|------------|-----------------------------------------------------
|32 B       |`recipient` |address identifier of the receiver
|variable   |`packet`    |encrypted `Packet` using publick key of the receiver
|96 B       |`seal`      |optional `Seal` signature

### 3.2.2.1 Seal

|Size       |Field       |Usage
|-----------|------------|-----------------------------------------------------
|32 B       |`source`    |address identifier of the source
|64 B       |`signature` |the signature of the `source` for a `packet`


## 3.3 Transport Layer

### 3.3.1 Datagram

The datagram is the highest unit of communication in the protocol, a datagram
contains a payload with some headers to be transported by the network.

|Size       |Field       |Usage
|-----------|------------|-----------------------------------------------------
|variable   |`headers`   |a set of `Header` storing the metadata
|variable   |`payload`   |an array of bytes storing the content

### 3.3.1 Header

|Size       |Field       |Usage
|-----------|------------|-----------------------------------------------------
|32 B       |`uuid`      |an identifier for the header
|variable   |`value`     |an array of bytes storing the value

The Nil UUID is reserved for the system and must no be set by applications.

## 3.4 System Datagrams

The system datagrams are special datagrams which are produced by the system to
provide transport services.

To be considered as such, a datagram must include a header with a Nil UUID
where the value is a ASCII string specifying the topic of the datagram.

|Topic                 |Usage
|----------------------|---------------------------------------------------------------
|net.announce.address  |announce an address identifer must be encrypted with the target relay key
|net.announce.carrier  |announce a carrier which must be encrypted with the target relay key
|net.announce.relay    |announce a relay
|net.announce.tree     |announce a tree
|net.beam              |broadcast an arbitrary datagram in the connected vortices

### 3.4.1 net.announce.address

The `announce.address` datagrams must be encapsulated for a given relay.

|Size       |Field       |Usage
|-----------|------------|-----------------------------------------------------
|32 B       |`address`   |the public key identifier of the address

### 3.4.2 net.announce.bridge

The `announce.bridge` datagrams are brodcasted publicly to every members of a
vortex.

|Size       |Field       |Usage
|-----------|------------|-----------------------------------------------------
|32 B       |`contact`   |the public key of the contact
|variable   |`spans`     |a set of `Span` part of the bridge
|64 B       |`signature` |an optional signature of the `contact` of the bridge for `spans`

The field `signature` must not be defined when a peer request the bridge to the
contact but it must be defined when the contact push back an authorized bridge.

#### 3.4.2.1 Span

|Size       |Field       |Usage
|-----------|------------|-----------------------------------------------------
|32 B       |`vortex`    |the public key identifier of the vortex
|8 B        |`timestamp` |the epoch under which the span was authored
|8 B        |`revolution`|the revolution under which the span was authored
|64 B       |`signature` |the signature of the `vortex` for fields `bridge.contact`, `timestamp` and `revolution`


### 3.4.3 net.announce.carrier

The `announce.carrier` datagrams must be encapsulated for a given relay.

|Size       |Field       |Usage
|-----------|------------|-----------------------------------------------------
|variable   |`carrier`   |the `Connection` of the carrier

### 3.4.4 net.announce.relay

The `announce.relay` datagrams are brodcasted publicly to every members of a
vortex.

|Size       |Field       |Usage
|-----------|------------|-----------------------------------------------------
|variable   |`relay`     |the `Connection` of the relay

### 3.4.5 net.announce.tree

The `announce.tree` datagrams are brodcasted publicly to every members of a
vortex.

|Size       |Field       |Usage
|-----------|------------|-----------------------------------------------------
|32 B       |`contact`   |the public key of the contact
|variable   |`tree`      |a `Tree` representing a partial view of the network
|8 B        |`timestamp` |the epoch under which the tree was authored
|64 B       |`signature` |the signature of the `contact` for the fields `tree` and `timestamp`

### 3.4.6 net.beam

This topic is used to broadcast an arbitrary capsule publicly to nearby peers
and their connected vortices.

The datagram is repeatedly broadcasted by each peer during a certain amount of
time which is define as a parameter of the system.

This should be exposed at the application layer to provide a means of
exchanging secrets, envelopes and any other data between nearby peers without
requiering the knowledge of network addresses.

|Size       |Field       |Usage
|-----------|------------|-----------------------------------------------------
|32 B       |`contact`   |the public key of the contact
|variable   |`capsule`   |the `Capsule`
|variable   |`vortices`  |a set of vortex public keys
|64 B       |`signature` |the signature of the `contact` for the fields `capsule` and `vortices`

## 3.5 Transport Data Model

### 3.5.1 Connection

|Size       |Field       |Usage
|-----------|------------|-----------------------------------------------------
|32 B       |`contact`   |the public key of the contact
|32 B       |`vortex`    |the public key of the vortex this contact originate
|variable   |`histogram` |a set of carrier histogram signed by relays
|8 B        |`timestamp` |the epoch under which the contact was initiated
|64 B       |`signature` |the signature of the `contact` for the fields `vortex`, `histogram` and `timestamp`

### 3.5.2 Envelope

|Size       |Field       |Usage
|-----------|------------|-----------------------------------------------------
|32 B       |`address`   |the public key of the address
|variable   |`flock`     |the private flock key of the address  (optional)
|variable   |`relays`    |a set of relay public key identifier where messages should exit

The `flock` fields is optional,  when it is defined it allows the owners of the
envelope to announce trees and carry messages on behalf of the address.

### 3.5.3 Histogram

|Size       |Field       |Usage
|-----------|------------|-----------------------------------------------------
|32 B       |`relay`     |the public key identifier of the relay
|variable   |`bins`      |the histogram bins holding the weights for this carrier
|8 B        |`timestamp` |the epoch under which this histogram was created
|64 B       |`signature` |the signature of the `relay` for the fields `bins` and `timestamp`

The bins are encoded as a list of double representing the weight for this
carrier over last `n` epochs, where `n` grow exponentially with the index of
the list.

The timestamp unit should match the one used for the epoch of the system,
typically a count of the number of days since 1st of January 2020 UTC.

### 3.5.3 Tree

|Size       |Field       |Usage
|-----------|------------|-----------------------------------------------------
|32 B       |`node`      |the public key of the contact at this node
|8 B        |`timestamp` |the epoch under which the linkprint between the trunk and this node was created
|64 B       |`signature` |the linkprint signature of the `node` of the trunk for the fields `node` and `timestamp`
|variable   |`branches`  |a set of `Tree`s connected to this node

## 3.6 Cryptographic Keys and Identifiers

### 3.6.1 PublicKeyID

The identifier of a public key, it is encoded as a SHA-256 hash of the public
key bytes.

### 3.6.2 PublicKey

The Ed25519 public key of a key pair, represented as a string of 32 bytes.

### 3.6.3 PrivateKey

The Ed25519 private key of a key pair, represented as a variable string of
bytes.

# 4 Protocol

## 4.1 Parameters

We define parameters that must be fixed for the system and must be the same for
every peers participating in the network.

### 4.1.1 Link Parameters

#### 4.1.1.1 Cell Size

The number of frames a cell must contain to be valid.

#### 4.1.1.2 Discovery Frequency

The number of times a discovery session will be initiated during an epoch.

#### 4.1.1.2 Discovery Time

The duration of a discovery session.

#### 4.1.1.3 Frame Size

The frame size is the sum of the *frame metadata size* and the *frame bucket
size*, they express the exact size in bytes those datastructure must have after
padding.

### 4.1.1.4 Vortex Grow Limit

The number of revolution a vortex can spin before being locked.

### 4.1.1.4 Vortex Life Limit

The number of revolution a vortex can spin before being terminated.

### 4.1.1.5 Vortex Members Limit

The number of members a vortex can contain before being full.

### 4.1.1.6 Vortex Revolution Time

The duration of a vortex revolution.

### 4.1.2 Transport Parameters

### 4.1.2.1 Bridge Life Limit

The number of revolution a bridge can be in use before requiering a refresh.

## 4.2 Vortex Protocol

### 4.2.1 Discovery Session

During an epoch, at every moments defined by the *discovery frequency*,
every peers must listen for vortices cells for the duration given by
*discovery time*.

After gathering the cells, the peers must join all the vortices they
possibly can while staying in the vortices for which they were already members.

If at the end of a discovery session, a peer is not a member of any vortices,
the peer must initiate a vortex during the next discovery session.

### 4.2.3 Cryptographic Keys

When initiating or joining a vortex, a peer must generate a fresh key pair that
will be used to represent the peer as a member of this vortex.

When initiating a vortex, a peer generate a fresh key pair that will be used to
represent the vortex in the network. The key pair is derived from a random
numerical seed which is shared in the initial cell cipher.

When new members request to join the vortex, they must sign their member public
key using the vortex private key and use the result as their cell cipher.

If one or more join requests are considered valid, every members but the last
one (as defined by their order of arrival) generate a random number and share it
as their cell cipher at the next revolution. The last member have to gather all
the others cipher to generate its own cipher so the sum of all numerical ciphers
is equal to the vortex seed.

### 4.2.4 Revolution Iteration

A revolution starts when the pole member send its cell, once they receive the
cell from the pole every other members must broadcast their message after a
randomized delay and before the end of the vortex revolution time set for the
system, in no specific order.

At the next revolution, the pole pass to the next member rotating in the order
of arrival which is encoded in the field `members` of the cell.

The system parameters must be tuned so that the length of a discovery session is
at least longer than the time taken by one revolution.

### 4.2.5 Membership Consensus

When the pole generate its cell it should include members that sent a valid join
request during the previous iteration. The other members must eject the pole if
it include an invalid member or if it exclude a previously existing member that
did broadcast a valid and signed cell during the last revolution.

Similarly any member that broadcast an invalid but signed cell, or any member
that did not broadcast a cell during the revolution time must be ejected.

When a member is ejected its cell information must be ignored and the member
must be removed from the vortex at the next revolution.

A cell is considered invalid if any of fields `members`, `cipher` or `pole`
does not follow the rules of the protocol. A cell is ignored if it failed
signature or consistency check.

### 4.2.6 Lifecycle

When a vortex have less than three members, they must all remain passive by
only broadcasting noise and mixing frames from others connected vortices.

Above this limit, peers could be active and start broadcasting their own frames,
or wait for a higher number of members depending on user preferences.

An vortex must be locked if any of the following conditions are met:
-   members count have reach the vortex members limit of the sytem
-   revolution have reach the vortex grow limit of the system

When a vortex is locked it active remains but all its members must broadcast a
null `cipher` and join requests are ignored.

An active vortex must be terminated if any of the following conditions are met:
-   members count is below three
-   revolution have reach the vortex life limit of the system

When a vortex is terminated all its members should immeditaly stop broadcasting,
effectively terminating the vortex.

### 4.2.X Fork and Merge

The identity of vortex is composed of the public key of the vortex and the list
of its member. Due to network latency it is possible for multiple forks of the
same vortex public key to be active at the same time.

If at the end of a revolution of a vortex a peer detect that its members list is
a subset of an other connected vortex, the peer must terminate its membership
with the vortex having the smallest set of members. Following this process the
forks should eventually get merged together.

During the discovery session, vortices with more than a single valid fork must
be ignored.

### 4.2.7 Mixing

When participating in more than one vortex, peers must mix frames between the
different connected vortices, actively connecting the vortices together.

In addition, peers must be listening to cells of vortices for which their are
not actively taking part and mix their traffic with their source of noise.

# 4.3 Bridge Protocol

# 4.4 Relay Protocol

# 4.5 Beam Protocol

# 5 Security

(_contained in separate file_)

# Annex A. Physical layer specific details

TODO: Here we details security and anonymity concerns

TODO: Here we briefly describe concrete implementation (WiFi, LoRa, ...)

# Annex B. Protocol Buffers Schema

(_contained in separate file_)

# Annex C. Use Case Examples

## Use case 1: Electronic Mail

TODO: Here we describe an electronic mail application

## Use case 2: Social Network

TODO: Here we describe a scuttlebutt application

# Annex D. Definitions

Below is an overview of terms as used in this standard within the context of CODL.

Term                |Description
--------------------|---------------------------------------------------------------------------------------
Address             |The identifier that corresponds to a cryptographic key pair to receive _datagrams_, i.e. to receive _messages_, on the _network_.
Bridge              |A route in the vortices mesh leading to a _contact_.
Bucket              |A layer of _onion_ formed by a _frame_.
Capsule             |A layer of _onion_ formed by a _packet_.
Carrier             |The identifier that corresponds to a cryptographic key pair used by a _peer_ to exchange _capsules_ with a specific _relay_.
Connection          |A _contact_ participating in a _vortex_.
Contact             |A _carrier_ or _relay_ transporting _packets_ on the _network_.
Cell                |The basic unit of communication, it represents a set of _frames_ broadcasted by a _peer_ on a _vortex_.
Datagram            |The highest unit of communication, it represents data to be processed by the applications.
Epoch               |An arbitrary period of time which represent iteration of the system (typically a UTC day).
Envelope            |The secret information requiered to send messages to an _address_ on the _network_.
Frame               |An _onion_ containing a set of _packets_, eventually being routed to its _receiver_ by the link layer.
Initiator           |The _peer_ that started the initial _revolution_ of a _vortex_.
Link                |A pair of _relays_ which are intermittently connected by one or more _carriers_.
Member              |The identifier that corresponds to a cryptographic key pair for a _peer_ to broadcast _frames_ on a specific _vortex_.
Network             |All the _participants_.
Onion               |A message that is encapsulated in layers of encryption, analogous to layers of an onion.
Packet              |An _onion_ containing a set of _datagrams_, eventually being routed to its _recipient_ by the network layer.
Participant         |A person or an organisation using the CODL protocol; a participant may have multiple _addresses_ and/or _relays_.
Peer                |A device part of the _network_ and running the CODL protocol.
Pole                |The _member_ responsible for starting the next _revolution_.
Receiver            |A _member_ or _address_ which is the target of a _frame_.
Recipient           |An _address_ which is the target of a _packet_.
Relay               |A static _device_ exposing a public _address_ to relay _capsules_ on the _network_.
Revolution          |The iteration of a _vortex_.
Tree                |A _tree_ is a subset of the network graph which represents one or more paths that reach a specific _relay_.
User                |A living organism equipped with a mobile device roaming through the _network_ and owning at least one _address_.
Vortex              |A set of _peers_ cooperating to broadcast on a common communication channel.
