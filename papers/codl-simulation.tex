\documentclass{article}
\usepackage{tkz-graph}
\usepackage{booktabs}
\usetikzlibrary{shapes.geometric}
\title{A simulation model for CODL}
\date{2020-02-27\\\normalsize Version 0.1}
\author{Pata Gioenas\\patagioenas@ctemplar.com\\pipio.gitlab.io}
\begin{document}

\maketitle

\begin{abstract}
   We describe a simulation model for a system of intermittently connected peers that broadcast messages periodically using proximity-based device-to-device communication. We describe the performance metrics of the system. We discuss design trade-offs that are likely to be made during the implementation of a simulation engine using our model.
\end{abstract}

\section{Introduction}

We are interested in the discrete event-simulation of a communication system for mobile devices. The devices in our system can only broadcast messages to other peers which are close enough to them.

In our simulation, we expect all peers to be synchronized and messages to always get delivered as long as devices are in range. We believe those issues can be ignored as they must be mitigated in the implementation and shouldn't have a significant impact on the performance.

We consider our system to form a fully connected network if every peer is able to eventually broadcast a message to every other peers.

\section{Definitions}

\subsection{Proximity distance}

We define the proximity distance \(d\) to represent the distance between any two peers of our system.

We define \(D\) as being the maximum distance there can be between two peers in order for them to exchange information successfully. We use the term {\it nearby peers}, to describe all peers with a proximity distance smaller than or equal to \(D\) relative to a given peer.

\subsection{Epoch and cycles}

We define an {\it epoch} to represent our basic unit of time, typically a day, which is then subdivided into multiple {\it cycles}.

At the start of each cycle all peers initiate a communication session.

\subsection{Communication session}

A {\it communication session} can last one or more rounds, during each rounds every peers will send all the messages from their outboxes to their nearby peers and simultaneously receive all incoming messages in their inboxes.

\subsection{Proximity context}

We define a {\it proximity context} as an isolated context of proximity where a certain number of peers move and occupy spots following a specific network.

It is encoded as a connected graph where vertexes are projected in a Cartesian coordinate system. Such a context is typically used to model rooms, platforms, and carriages.

We classify vertexes in two categories, {\it occupancy spots} which represents a location that a peer could occupy, and {\it waypoints} which are used to route peers between occupancy spots. We distinguish waypoints in two sub-categories, {\it gate} which represents vertexes that should be used to enter or exit the context, and {\it hub} which are used only for internal routing.

We define the {\it capacity} of a context as the number of occupancy spots it contains.

\begin{figure}[h]
\centering
\begin{tikzpicture}

  \SetGraphUnit{2}
  \draw[help lines, color=gray!30, dashed] (-2,-2) grid (2,2);

  \Vertex[x= 0, y= 0]{H}
  \Vertex[x= 0, y=-1]{G}
  \tikzset{VertexStyle/.append style={rectangle}}
  \Vertex[x= 0, y= 1]{N}
  \Vertex[x=-1, y= 0]{W}
  \Vertex[x= 1, y= 0]{E}

  \Edge(G)(H)
  \Edge(H)(N)
  \Edge(H)(W)
  \Edge(H)(E)
\end{tikzpicture}
\caption{A simple proximity context consisting of two waypoints (\(G\)ate and \(H\)ub) which route peers into three occupancy spots (\(N\), \(W\) and \(E\)).}
\label{fig:proximityContext}
\end{figure}

\newpage

\subsection{Mobility network}

We define a {\it mobility network} as a set of proximity contexts arbitrarily connected by their gates.

It is encoded as a graph where vertexes can be connected to proximity contexts gates. Those vertexes forms a unique proximity context which consists exclusively of waypoints and cannot be occupied.

\begin{figure}[h]
\centering
\begin{tikzpicture}

  \SetGraphUnit{2}
  \draw[help lines, color=gray!30, dashed] (-2,-3) grid (8,2);

    \tikzset{VertexStyle/.append style={color=blue, fill=white, text=black}}
  \Vertex[x= 0, y= 0]{H}
  \Vertex[x= 0, y=-1]{G}
  \tikzset{VertexStyle/.append style={rectangle}}
  \Vertex[x= 0, y= 1]{N}
  \Vertex[x=-1, y= 0]{W}
  \Vertex[x= 1, y= 0]{E}
    \tikzset{EdgeStyle/.append style={color=blue}}
  \Edge(G)(H)
  \Edge(H)(N)
  \Edge(H)(W)
  \Edge(H)(E)
      \tikzset{VertexStyle/.append style={circle, color=black, fill=white}}
   \Vertex[x= 3, y=-2, NoLabel]{X}
       \tikzset{EdgeStyle/.append style={color=black}}
   \Edge(G)(X)


    \tikzset{VertexStyle/.append style={color=red, fill=white, text=black}}
  \Vertex[x= 6, y= 0]{H}
  \Vertex[x= 6, y=-1]{G}
  \tikzset{VertexStyle/.append style={rectangle}}
  \Vertex[x= 6, y= 1]{N}
  \Vertex[x= 5, y= 0]{W}
  \Vertex[x= 7, y= 0]{E}
    \Edge(G)(X)
    \tikzset{EdgeStyle/.append style={color=red}}
  \Edge(G)(H)
  \Edge(H)(N)
  \Edge(H)(W)
  \Edge(H)(E)
\end{tikzpicture}
\caption{A mobility network interconnecting one proximity context (in blue) to an other one (in red) threw their respective gates.}
\label{fig:mobilityGraph}
\end{figure}

\subsection{Peer schedule}

We define a {\it peer schedule} as a representation of the path that a given peer will take in the mobility network for the duration of an epoch.

It is encoded a pair of time-stamped locations, representing the initial and the final location of the peer at the begin and the end of the travel respectively, and a route which is encoded as a list of pairs consisting of a location and a time interval.

A location can reference either an occupancy spot or a whole proximity context in which case the allocation will be handled dynamically.

\begin{table}[h]
\centering
\begin{tabular}{llll}
\toprule
Peer  & Initial & Final & Route \\
\midrule
Alice & (07:00, {\color{blue} W}) & (12:00, {\color{red} W}) &  [(07:30-07:45, {\color{red} N}), 07:50-11:55, {\color{red} E})]\\
Bob   & (07:20, {\color{blue} N}) & (07:30, {\color{red} ?}) &  nil \\
\hline
\end{tabular}
\caption{The schedules of Alice and Bob, we use the symbol {\it ?} to denote the dynamic allocation of Bob's final location.}
\label{fig:peerSchedule}
\end{table}

\subsection{Transport network}

We define a {\it transport network} as layer on a given mobility graph that allow transits between proximity contexts it connects. Every contexts of a given network must be identical in their local projection.

It is encoded as a graph where vertexes can be connected to proximity contexts of the underlying mobility graph.

The proximity contexts which are included in a transport network don't have a static instance during simulation and do instead represents anchors where instances can be attached dynamically.

\begin{figure}[h]
\centering
\begin{tikzpicture}
  \tikzset{EdgeStyle/.append style={dashed}}
  \Vertex[x=0, y=1.5]{A}
  \Vertex[x=0, y=0.5]{B}
  \Vertex[x=1, y=1]{O}
  \Vertex[x=2, y=1]{P}
  \Vertex[x=3, y=1.5]{X}
  \Vertex[x=3, y=0.5]{Y}
  \Edge(A)(O)
  \Edge(B)(O)
  \Edge(O)(P)
  \Edge(P)(X)
  \Edge(P)(Y)
\end{tikzpicture}
\caption{A simple transport network interconnecting six proximity contexts}
\label{fig:transportNetwork}
\end{figure}

\subsection{Transport stream}

We define a {\it transport stream} as a proximity context transiting on a given transport network by following a certain path for the duration of an epoch.

It is encoded a pair of time-stamped locations, representing the initial and the final context of the stream at the begin and the end of the travel respectively, and a route which is encoded as a list of pairs consisting of a reference to a context and a time interval.


\begin{table}[h]
\centering
\begin{tabular}{llll}
\toprule
Stream  & Initial & Final & Route \\
\midrule
S0 & (07:00, A) & (07:30, Y) &  [(07:08-07:12, O), (07:18-07:22, P)]\\
S1   & (07:00, X) & (07:30, B) & [(07:08-07:12, P), (07:18-07:22, O)] \\
\hline
\end{tabular}
\caption{Two streams running on the network of Figure~\ref{fig:transportNetwork}}
\label{fig:transportStream}
\end{table}


\subsection{Proximity graph}

In a {\it proximity graph}, we consider two peers to be connected if their Cartesian distance is smaller than or equal to \(D\).

\begin{figure}[h]
\centering
\begin{tikzpicture}
  \GraphInit[vstyle=Classic]
  \Vertex[Lpos=60, x=0]{A}
  \Vertex[Lpos=60, x=2]{B}
  \Edge(A)(B)
\end{tikzpicture}
\caption{\(A\)lice and \(B\)ob are connected, they can discover each others and successfully initiate a communication session.}
\label{fig:proximityGraph}
\end{figure}

The proximity graph of the system changes over time according to the peer schedules, the main task of the simulation engine is to produce a proximity graph at the start of each communication session.

\subsection{IDENTITY}

We define the protocol {\it IDENTITY} as a simplified version of {\it CODL}, it is designed for performance analysis and has no real-world application.

At the begin of the simulation, each peer initializes his outbox with a single message containing his identifier. A communication session is finished after a single round of messages exchange. At the end of each communication session, each peer removes all the messages from his inbox and add them, if not already present, in his outbox.

\section{Performance metrics}

We describe the {\it full network} and the {\it peer-to-peer} propagation time as performance metrics of our system. Those propagation times are expressed as duration in epochs and cycles.

\subparagraph{Full network.} The time it takes for every peers of the system to have received the identifiers of every other peers of the system.

\subparagraph{Peer-to-peer.} The time it takes for a target peer to receive the identifier of a source peer.

\section{Design}

We consider the simulation of {\it IDENTITY} using our model.

\subsection{Dynamic occupancy allocation}

We observe that many common human mobility behaviors for both public transport (carriages, platforms or waiting areas) and indoor mobility (rooms, lifts or meeting points) could be modeled as proximity context that uses occupancy allocation strategies relying on probability distributions. Those distributions could be inferred from real-world observations.

We believe that those domain-specific contexts can be designed in such a modular fashion that they could be composed together to form higher-level representation of complex human mobility behavior like transport networks, office towers or residential areas.

\subsection{Groundtruthing and procedural generation}

We observe that large scale models could be produced using procedures that combine domain-specific contexts by following patterns modeled from the real-world. Those patterns generally exhibits self-similarity and are well suited for procedural generation of large scale systems (towns, cities and countries).

In order to provide realistic mobility pattern for those large scale systems, the scheduling of peers must be handled dynamically. Probabilistic models could be build to infer peer schedules based on real-world observations.

\subsection{Collision detection and lazy projection}

We believe that collision detection between peers is irrelevant to the estimation of our performance metrics and we propose to build the simulation engine without any collision detection algorithm.

Instead, we propose to attach and track peers on path and occupancy spots. We could then project their coordinate only when a proximity graph needs to be produced. At this time, overlapping peers must be resolved and could be projected using strategies modeled from real-world spatial distributions.

\subsection{Scalability}

For large scale systems with sufficiently detailed contexts, we believe that simulating only peers that are attached to occupancy spots is enough to evaluate performance metrics. We could then ignore altogether the spatial projection of the peers and rely instead on a set of proximity graphs precomputed for each proximity contexts.

\end{document}
