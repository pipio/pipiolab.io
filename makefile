SHELL=/usr/bin/env bash

all:
	cd projects/p3o && $(MAKE)
precommit:
	cd projects/p3o && $(MAKE) precommit
