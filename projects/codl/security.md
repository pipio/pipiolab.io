# Threat Model

## Censorship Threats

TODO: Flood a peer with fake vortices
  - risk: cannot connect to the mesh and see bridges
  - risk: leak address identity
  - risk: leak peer identify if not stealth
  - mitigation: rate limit joining new vortices
  - mitigation: priority to stay in large/old vortices with bridges

TODO: Flood a vortex with fake peers
  - risk: leak address identify
  - risk: decomission prematuraly healthy vortices
  - mitigation: rate limit joining new members proportionaly to vortex age
  - mitigation: priority on old members when routing own announces in vortex

TODO: Sybill attack overflowing relays and peers
TODO: Relay attack remote vortex with beam flooding

## Privacy Threats

TODO:
- Two or more relays collaborating to trace a carrier
    - mitigation 1: relay broadcast message to other relay publicly, might be peeked by other carrier going thru similar link
    - mitigation 2: carrier might join other vortices along the way and mix message with other carriers
    - mitigation 2: flock mixing of contact id for social user
